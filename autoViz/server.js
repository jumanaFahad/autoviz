const express = require('express');
const path = require('path');

const app = express();

// app.use(express.static(path.resolve("./")));
const static = express.static(path.join(__dirname, '/public'));
app.use("/public", static);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));

});

app.use("*", (req, res) => {
    res.status(404).json({error: "Not found"});
});

app.listen(3000, () => {
    console.log("Auto Vis is running on http://localhost:3000");
});
