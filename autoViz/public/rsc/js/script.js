// This code will run after document is ready.
$(() => {
    openJSON('/public/data/VIZS.json')
    .then((data) => {
        let VIZS = data;
        const ROOT = $( '#root' );

        for (let i = 0; i < VIZS.length; i++) {
            let vizId = 'viz-' + i;
            let vizDataFile = '/public/data/' + VIZS[i].vizDataFile;
            let vizDiv = insertVizDOM(ROOT, vizId, VIZS[i].title, VIZS[i].subTitle);

            // Since d3 can't locate elemnt added dynamcily to the document
            // we needed to refrecne the elemnt through passing the jquery elemnt
            // to a d3 select
            let vizD3 = d3.select(vizDiv.get(0));
            createViz(vizD3, VIZS[i].vizType, vizDataFile, VIZS[i].vizValues);

            if( i < (VIZS.length-1) ) {
                ROOT.append('<hr/>');
            }
        }

    })
});

/**
 * @param root - Jquery refrecne to the root div.
 * @param vizId - Id for the viz div.
 * @param title - Title text above the viz.
 * @param subTitle - sub title text under the viz.
 * @returns viz - refrecne to the viz div.
 */
function insertVizDOM(root, vizId, title, subTitle) {
    let rapperElement = $('<div></div>').addClass('row');
    let titleElement = $('<div></div>').addClass('text-center col-xs-12 theme-bg').append( $( '<h3></h3>' ).append(title) );
    let vizRapperElement = $('<div></div>').addClass('light-gray-bg col-xs-12');
    let viz = $( '<div></div>' ).addClass('chart-placeholder vcenter').attr('id', vizId)
    let vizElement = $('<div></div>').addClass('row').append( $( '<div></div>' ).addClass('col-xs-12 col-md-10 col-md-offset-1 center-text')
                        .append( viz ) );
    let subTitleElement = $('<div></div>').addClass('row').append( $( '<div></div>' ).addClass('text-center col-xs-12 col-md-10 col-md-offset-1')
                            .append($( '<p></p>' ).append(subTitle)) );
    vizRapperElement.append(vizElement).append(subTitleElement);
    rapperElement.append(titleElement).append( vizRapperElement );
    root.append(  rapperElement );
    return viz;
}

/**
 * @param vizDOM - D3 refrecne to the viz div.
 * @param vizType - Viz type such as "pie", "bar" or "tree_map".
 * @param vizDataFile - Location for the viz data file.
 * @param vizValues - Viz configurations.
 */
function createViz(vizDOM, vizType, vizDataFile, vizValues) {
    openJSON(vizDataFile)
    .then((data) => {
        vizValues.container = vizDOM;
        vizValues.data = data;
        vizFactory(vizType, vizValues);
    })
    .catch((err) => {
        console.error('ERROR:', err.text);
    });
}
