/**
 * @param vizType - Viz type such as "pie", "bar" or "tree_map".
 * @param vizValues - Viz configurations.
 * @throws Will throw an error if type is not vaild.
 */
 function vizFactory(vizType, vizValues) {
    switch (vizType) {
        case 'bar':
            vizBar(vizValues);
            break;
        case 'pie':
            vizPie(vizValues);
            break;
        case 'tree_map':
            vizTree(vizValues);
            break;
        default:
            let error = 'Viz: Type (' + vizType + ') is not a vaild viz type.'
            throw ({ text: error});
    }
}

function vizTree(vizValues) {
    var vizObj = d3plus.viz()
    .type('tree_map')
    .size(vizValues.size)

    viz(vizObj, vizValues);
}

function vizPie(vizValues) {
    var vizObj = d3plus.viz()
    .type('pie')
    .size(vizValues.size)

    viz(vizObj, vizValues);
}

function vizBar(vizValues) {
    var vizObj = d3plus.viz()
    .type('bar')
    .x(vizValues.x)
    .y(vizValues.y)

    viz(vizObj, vizValues);
}

/**
 * @param vizObj - d3plus object to display.
 * @param vizValues - Viz configurations.
 * @throws Will throw an error draw failed.
 */
function viz(vizObj, vizValues) {
    try {
        vizObj.container(vizValues.container)
        .data(vizValues.data)
        .id(vizValues.id)
        .depth(0)
        .axes({"background": {"color": "black"}})
        .background("black")
        .labels({"resize":false})
        .font({"size":18})
        .time(vizValues.time)
        .font({ "family": "Homenaje" })
        .color({"scale":["#D1A300","#83D2C8", "#FFC700",  "#32af9f","#FEE28C","#19574f"]})
        .color(vizValues.colorBasedOn)
        .legend({"size":15, "align":"end","labels":false, "filters":true })
        .draw();
    }
    catch(err) {
        throw ({ text: err.message});
    }
}
