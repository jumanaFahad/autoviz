/**
 * @param filename - path to the json file.
 * @returns Json file as JS object.
 * @throws Will throw an error if file not exist or error reading.
 */
 function openJSON(filename) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType("application/json");
        xhr.open('GET', filename, true);
        xhr.onload = function () {
          if (this.status >= 200 && this.status < 300) {
            resolve(JSON.parse(xhr.responseText));
          } else {
            reject({text: xhr.statusText});
          }
        };
        xhr.onerror = function () {
          reject({text: xhr.statusText});
        };
        xhr.send();
  });
}

function scroll(section) {
    var target = $("#"+section);
    var position = target.offset().top - $(".navbar-header").height();
    $('html, body').animate({
        scrollTop: position
    }, 1000);
    return false;
}
